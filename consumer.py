from kafka import KafkaConsumer
from pymongo import MongoClient
import json
import requests
consumer = KafkaConsumer(
    'event_test',
     bootstrap_servers=['localhost:9092'],
     auto_offset_reset='earliest',
     enable_auto_commit=True,
     group_id='my-group',
     value_deserializer=lambda x: json.loads(x.decode('utf-8')))
client = MongoClient('localhost:27017')
collection = client.numtest.numtest

headers = {'content-type': 'application/json'}

def add_random_events():
    for data in consumer:
        print(data.value,type(data.value))
        result = requests.post(url="http://127.0.0.1:5000/event", data=json.dumps(data.value), headers=headers)
        print('{} added'.format(result))
        print(result.text)

if __name__ == '__main__':
    add_random_events()
