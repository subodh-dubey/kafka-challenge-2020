from flask import Flask, jsonify, request, abort, render_template
from pymongo import MongoClient
from producer import produce

# creating a Flask app
app = Flask(__name__, template_folder='template')
client = MongoClient('localhost:27017')
collection = client.event_database.event


@app.route("/")
def home():
    '''
    To render home.html page
    :return: render home.html
    '''
    return render_template("home.html")


@app.route('/event', methods=['GET'])
def add_event():
    '''
    To records a new event on the server. Events should be formatted as JSON objects.:
    :return: newly added event JSON
    '''

    cur = collection.find()
    data = []
    for i in cur:
        del i['_id']
        data.append(i)
    return jsonify(data)


@app.route('/event', methods=['POST'])
def event():
    '''
    Tp return a JSON list of all events the server has received, in the order that they happened
    :return: list of events
    '''
    data = request.get_json()  # status code
    if data['type'].upper() not in ["INCREMENT", "DECREMENT"]:
        return 'The type must be either "INCREMENT" or "DECREMENT"', 422
    try:
        data['value'] = int(data['value'])
    except (ValueError, TypeError):
        return 'The event value must be an integer', 422
    collection.insert_one(data)
    del data['_id']
    return jsonify(data), 200


@app.route('/value', methods=['GET'])
def get_value():
    '''
    To return the current value of the system
    :return: current value
    '''

    sum = 0
    cur = collection.find()
    for i in cur:
        if i["type"].upper() == "INCREMENT":
            sum = sum + i["value"]
        else:
            sum = sum - i["value"]
    return str(sum), 200


@app.route('/value/<t>', methods=['GET'])
def get_value_id(t):
    '''
    To return the value that the system had after the ﬁrst t events have happened.
    - If the system has received no events, the value 0 is returned.
    - If t is less than 0, the function raise a 400 series error
    - If t is greater than the number of events, the function returns the "current" value

    :param t:
    :return: current integer value
    '''
    if (request.method == 'GET'):
        try:
            t = int(t)
        except ValueError:
            return str("The value of t must be integer")

        if t < 0:
            abort(400)
        sum = 0
        cur = collection.find()
        for i in cur:
            if t == 0:
                break
            if i["type"].upper() == "INCREMENT":
                sum = sum + i["value"]
            else:
                sum = sum - i["value"]
            t = t - 1
        return str(sum), 200


@app.route('/produce', methods=['GET'])
def producer():
    '''
    To POST random events type and random value (between 1 and 5) to the /event endpoint every X ms.
    :return: produced 20 random events
    '''
    if (request.method == 'GET'):
        events = produce()
        return jsonify(events), 200

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
