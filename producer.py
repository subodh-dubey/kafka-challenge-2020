import random
import json
from time import sleep
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                         value_serializer=lambda x: json.dumps(x).encode('utf-8'))

# to produce 20 random events and brodcast to API
def produce():
    events = []
    for e in range(20):
        types = ['INCREMENT','DECREMENT']
        type = random.choice(types)
        value = random.randint(1,5)
        data = {'type' : type, 'value': value}
        print(data)
        events.append(data)
        producer.send('event_test', value=data)
        sleep(5)
    return events

if __name__ == '__main__':
    produce()


